package com.example.epsi_soap_rest.controllers;

import com.example.epsi_soap_rest.model.ArticleCategory;
import com.example.epsi_soap_rest.repositories.ArticleCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categorie_article")
public class ArticleCategoryRestController {
  @Autowired
  private ArticleCategoryRepository articleCategoryRepository;


  @GetMapping
  public List<ArticleCategory> getAllArticleCategories() {
    return articleCategoryRepository.findAll();
  }

  @GetMapping("/{id}")
  public ArticleCategory getArticleCategoryById(@PathVariable Long id) {
    return articleCategoryRepository.findById(id).orElse(null);
  }

  @PostMapping
  public ArticleCategory createArticleCategory(@RequestBody ArticleCategory articleCategory) {
    return articleCategoryRepository.save(articleCategory);
  }

  @PutMapping("/{id}")
  public ArticleCategory updateArticleCategory(@PathVariable Long id, @RequestBody ArticleCategory articleCategoryDetails) {
    ArticleCategory articleCategory = articleCategoryRepository.findById(id).orElse(null);
    if (articleCategory != null) {
      articleCategory.setNom(articleCategoryDetails.getNom());
      return articleCategoryRepository.save(articleCategory);
    }
    return null;
  }

  @DeleteMapping("/{id}")
  public void deleteArticleCategory(@PathVariable Long id) {
    articleCategoryRepository.deleteById(id);
  }

}


