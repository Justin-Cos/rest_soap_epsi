package com.example.epsi_soap_rest.controllers;

import com.example.epsi_soap_rest.model.Stock;
import com.example.epsi_soap_rest.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/stocks")
public class StockRestController {

    @Autowired
    private StockRepository stockRepository;

    @GetMapping
    public List<Stock> getAllStocks() {
        return stockRepository.findAll();
    }

    @GetMapping("/{id}")
    public Stock getStockById(@PathVariable Long id) {
        return stockRepository.findById(id).orElse(null);
    }

    @PostMapping
    public Stock createStock(@RequestBody Stock stock) {
        return stockRepository.save(stock);
    }

    @PutMapping("/{id}")
    public Stock updateStock(@PathVariable Long id, @RequestBody Stock updatedStock) {
        Stock stock = stockRepository.findById(id).orElse(null);
        if (stock != null) {
            stock.setId_article(updatedStock.getId_article());
            stock.setAdresse(updatedStock.getAdresse());
            stock.setQuantite(updatedStock.getQuantite());
            stock.setCreated_at(updatedStock.getCreated_at());
            stock.setUpdated_at(updatedStock.getUpdated_at());
            return stockRepository.save(stock);
        }
        return null; // Ou gérer l'erreur selon le besoin
    }

    @DeleteMapping("/{id}")
    public void deleteStock(@PathVariable Long id) {
        stockRepository.deleteById(id);
    }
}
