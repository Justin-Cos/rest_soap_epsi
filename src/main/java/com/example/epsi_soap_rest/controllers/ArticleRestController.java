package com.example.epsi_soap_rest.controllers;

import com.example.epsi_soap_rest.model.Article;
import com.example.epsi_soap_rest.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/articles")
public class ArticleRestController {
  @Autowired
  private ArticleRepository articleRepository;


  @GetMapping
  public List<Article> getAllArticles() {
    return articleRepository.findAll();
  }

  @GetMapping("/{id}")
  public Article getArticleById(@PathVariable Long id) {
    return articleRepository.findById(id).orElse(null);
  }

  @PostMapping
  public Article createArticle(@RequestBody Article article) {
    return articleRepository.save(article);
  }

  @PutMapping("/{id}")
  public Article updateArticle(@PathVariable Long id, @RequestBody Article articleDetails) {
    Article article = articleRepository.findById(id).orElse(null);
    if (article != null) {
      article.setNom(articleDetails.getNom());
      article.setDescription(articleDetails.getDescription());
      article.setImage(articleDetails.getImage());
      article.setTitle(articleDetails.getTitle());
      article.setContent(articleDetails.getContent());
      return articleRepository.save(article);
    }
    return null;
  }

  @DeleteMapping("/{id}")
  public void deleteArticle(@PathVariable Long id) {
    articleRepository.deleteById(id);
  }

}


