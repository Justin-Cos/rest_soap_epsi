package com.example.epsi_soap_rest.controllers;

import com.example.epsi_soap_rest.model.Location;
import com.example.epsi_soap_rest.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/locations")
public class LocationRestController {
  @Autowired
  private LocationRepository locationRepository;


  @GetMapping
  public List<Location> getAllLocations() {
    return locationRepository.findAll();
  }

  @GetMapping("/{id}")
  public Location getLocationById(@PathVariable Long id) {
    return locationRepository.findById(id).orElse(null);
  }

  @PostMapping
  public Location createLocation(@RequestBody Location location) {
    return locationRepository.save(location);
  }

  @PutMapping("/{id}")
  public Location updateLocation(@PathVariable Long id, @RequestBody Location locationDetails) {
    Location location = locationRepository.findById(id).orElse(null);
    if (location != null) {
      location.setDate_debut(locationDetails.getDate_debut());
      location.setDuree_jours(locationDetails.getDuree_jours());
      location.setCreated_at(locationDetails.getCreated_at());
      location.setUpdated_at(locationDetails.getUpdated_at());
    }
    return null;
  }

  @DeleteMapping("/{id}")
  public void deleteLocation(@PathVariable Long id) {
    locationRepository.deleteById(id);
  }

}


