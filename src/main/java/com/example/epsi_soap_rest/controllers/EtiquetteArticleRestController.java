package com.example.epsi_soap_rest.controllers;

import com.example.epsi_soap_rest.model.EtiquetteArticle;
import com.example.epsi_soap_rest.repositories.EtiquetteArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/etiquettes_articles")
public class EtiquetteArticleRestController {

    @Autowired
    private EtiquetteArticleRepository etiquetteArticleRepository;

    @GetMapping
    public List<EtiquetteArticle> getAllEtiquettesArticles() {
        return etiquetteArticleRepository.findAll();
    }

    @GetMapping("/{id}")
    public EtiquetteArticle getEtiquetteArticleById(@PathVariable Long id) {
        return etiquetteArticleRepository.findById(id).orElse(null);
    }

    @PostMapping
    public EtiquetteArticle createEtiquetteArticle(@RequestBody EtiquetteArticle etiquetteArticle) {
        return etiquetteArticleRepository.save(etiquetteArticle);
    }

    @PutMapping("/{id}")
    public EtiquetteArticle updateEtiquetteArticle(@PathVariable Long id, @RequestBody EtiquetteArticle updatedEtiquetteArticle) {
        EtiquetteArticle etiquetteArticle = etiquetteArticleRepository.findById(id).orElse(null);
        if (etiquetteArticle != null) {
            etiquetteArticle.setNom(updatedEtiquetteArticle.getNom());
            return etiquetteArticleRepository.save(etiquetteArticle);
        }
        return null; // Ou gérer l'erreur selon le besoin
    }

    @DeleteMapping("/{id}")
    public void deleteEtiquetteArticle(@PathVariable Long id) {
        etiquetteArticleRepository.deleteById(id);
    }
}
