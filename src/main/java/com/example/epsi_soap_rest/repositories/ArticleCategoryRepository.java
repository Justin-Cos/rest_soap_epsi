package com.example.epsi_soap_rest.repositories;

import com.example.epsi_soap_rest.model.Article;
import com.example.epsi_soap_rest.model.ArticleCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleCategoryRepository extends JpaRepository<ArticleCategory, Long> {
}
