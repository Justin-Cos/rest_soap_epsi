package com.example.epsi_soap_rest.repositories;

import com.example.epsi_soap_rest.model.EtiquetteArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtiquetteArticleRepository extends JpaRepository<EtiquetteArticle, Long> {
}
