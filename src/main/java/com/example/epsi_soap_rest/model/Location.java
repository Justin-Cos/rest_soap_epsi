package com.example.epsi_soap_rest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Location {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Date date_debut;
  private Long duree_jours;
  private Date created_at;
  private Date updated_at;

  // Constructors, getters, and setters
  public Location() {}

  public Location( Date date_debut, Long duree_jours, Date created_at, Date updated_at) {
    this.date_debut = date_debut;
    this.duree_jours = duree_jours;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDate_debut() {
    return date_debut;
  }

  public void setDate_debut(Date date_debut) {
    this.date_debut = date_debut;
  }

  public Long getDuree_jours() {
    return duree_jours;
  }

  public void setDuree_jours(Long duree_jours) {
    this.duree_jours = duree_jours;
  }

  public Date getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Date created_at) {
    this.created_at = created_at;
  }

  public Date getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }
}
