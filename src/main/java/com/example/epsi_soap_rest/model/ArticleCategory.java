package com.example.epsi_soap_rest.model;

import javax.persistence.*;

@Entity
@Table (name="categorie_article")
public class ArticleCategory {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String nom;


  // Constructors, getters, and setters
  public ArticleCategory() {}

  public ArticleCategory( String nom) {
    this.nom = nom;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }
}
