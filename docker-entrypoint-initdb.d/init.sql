-- Créer la table des catégories d'article
CREATE TABLE categorie_article (
                                 id SERIAL PRIMARY KEY,
                                 nom VARCHAR(255) NOT NULL
);

-- Créer la table des étiquettes d'article
CREATE TABLE etiquette_article (
                                 id SERIAL PRIMARY KEY,
                                 nom VARCHAR(255) NOT NULL
);

-- Créer la table des articles
CREATE TABLE article (
                       id SERIAL PRIMARY KEY,
                       nom VARCHAR(255) NOT NULL,
                       id_categorie INT REFERENCES categorie_article(id),
                       id_etiquette INT REFERENCES etiquette_article(id),
                       description TEXT,
                       lien_image VARCHAR(255),
                       created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                       updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Créer la table des clients
CREATE TABLE client (
                      id SERIAL PRIMARY KEY,
                      nom VARCHAR(255) NOT NULL,
                      email VARCHAR(255) UNIQUE,
                      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                      updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Créer la table du stock
CREATE TABLE stock (
                     id SERIAL PRIMARY KEY,
                     id_article INT REFERENCES article(id),
                     adresse VARCHAR(255),
                     quantite INT NOT NULL,
                     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                     updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Créer la table des locations
CREATE TABLE location (
                        id SERIAL PRIMARY KEY,
                        id_client INT REFERENCES client(id),
                        id_article_emprunte INT REFERENCES article(id),
                        date_debut DATE,
                        duree_jours INT,
                        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Créer la fonction pour mettre à jour updated_at automatiquement
CREATE OR REPLACE FUNCTION update_updated_at_column()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = CURRENT_TIMESTAMP;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Créer le déclencheur pour appeler la fonction avant chaque mise à jour
CREATE TRIGGER set_updated_at_article
  BEFORE UPDATE ON article
  FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

-- Créer le déclencheur pour appeler la fonction avant chaque mise à jour
CREATE TRIGGER set_updated_at_stock
  BEFORE UPDATE ON stock
  FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

-- Créer le déclencheur pour appeler la fonction avant chaque mise à jour
CREATE TRIGGER set_updated_at_location
  BEFORE UPDATE ON location
  FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

-- Créer le déclencheur pour appeler la fonction avant chaque mise à jour
CREATE TRIGGER set_updated_at_client
  BEFORE UPDATE ON client
  FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();
